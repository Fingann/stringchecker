package main

import (
	"bufio"
	"bytes"
	"encoding/base64"
	"encoding/hex"
	"flag"
	"fmt"
	"log"
	"os"
)

var (
	filePath    string
	patternPath string
	patterns    []string
)

type Results map[string][]IndexMatch

func (s StrategyResults) Print() {
	fmt.Println("Strategy: ", s.Name)
	for _, result := range s.PatternResults {
		if len(result.Matches) == 0 {
			continue
		}
		fmt.Printf("Found %s: \n", result.Pattern)
		for _, matches := range result.Matches {
			fmt.Println(matches.index, ": ", matches.sample)
		}
	}
}

func main() {
	fmt.Println(hex.EncodeToString([]byte("HexTestValue")))
	file, patterns := ParseArguments()
	results := PlaintextSearch(file, patterns)
	results.Print()
	fmt.Println()
	results2 := Base64Search(file, patterns)
	results2.Print()
	results3 := HexSearch(file, patterns)
	results3.Print()
	fmt.Println()
}
func (pr PatternResults) Add() {

}

type PatternTransformer func(pattern Pattern) ([]byte, error)

func TransformerSearch(file []byte, patterns []Pattern, transformer PatternTransformer) PatternResults {

	results := make([]PatternResult, 0, len(patterns))
	for _, pattern := range patterns {
		transformed, err := transformer(pattern)
		if err != nil {
			log.Fatalln(err)
		}
		index := 0
		result := PatternResult{
			Pattern: pattern,
		}
		for {
			foundIndex := bytes.Index(file[index:], transformed)
			if foundIndex == -1 {
				break
			}

			sample := CreateSample(file, foundIndex, len(transformed))
			result.Matches = append(result.Matches, IndexMatch{
				index:  foundIndex,
				sample: sample,
			})

			index = index + foundIndex + len(transformed)
		}
		results = append(results, result)
	}
	return results

}

func Base64Search(file []byte, patterns []Pattern) StrategyResults {
	base64Transformer := func(pattern Pattern) ([]byte, error) {
		b := make([]byte, base64.StdEncoding.EncodedLen(len(pattern)))
		base64.StdEncoding.Encode(b, pattern)
		return b, nil
	}
	return StrategyResults{
		Name:           "Base64",
		PatternResults: TransformerSearch(file, patterns, base64Transformer),
	}
}

func HexSearch(file []byte, patterns []Pattern) StrategyResults {
	HexTransformer := func(pattern Pattern) ([]byte, error) {
		b := make([]byte, hex.EncodedLen(len(pattern)))
		hex.Encode(b, pattern)
		return b, nil
	}
	return StrategyResults{
		Name:           "Hex",
		PatternResults: TransformerSearch(file, patterns, HexTransformer),
	}
}

func PlaintextSearch(file []byte, patterns []Pattern) StrategyResults {
	plaintextTransformer := func(pattern Pattern) ([]byte, error) {
		return pattern, nil
	}
	return StrategyResults{
		Name:           "PlainText",
		PatternResults: TransformerSearch(file, patterns, plaintextTransformer),
	}
}

func CreateSample(file []byte, foundIndex, patternLength int) string {
	beforeIndex := foundIndex - 20
	if beforeIndex < 0 {
		beforeIndex = 0
	}

	afterIndex := foundIndex + patternLength + 20
	if afterIndex > len(file) {
		afterIndex = len(file)
	}
	return string(file[beforeIndex:afterIndex])
}

func ParseArguments() ([]byte, []Pattern) {
	flag.StringVar(&filePath, "f", "", "File to be checked")
	flag.StringVar(&patternPath, "p", "", "File containing the patterns to match against.")
	flag.Parse()
	file, err := os.ReadFile(filePath)
	if err != nil {
		log.Fatalln(err)
	}
	patterns := ParsePatternFile()
	return file, patterns
}

func ParsePatternFile() []Pattern {
	patternsFile, err := os.Open(patternPath)
	if err != nil {
		log.Fatalln(err)
	}
	scanner := bufio.NewScanner(patternsFile)
	scanner.Split(bufio.ScanLines)
	var patterns []Pattern
	for scanner.Scan() {
		patterns = append(patterns, scanner.Bytes())
	}
	return patterns
}

type Pattern []byte
type PatternResults []PatternResult

type StrategyResults struct {
	Name string
	PatternResults
}

type PatternResult struct {
	Pattern Pattern
	Matches []IndexMatch
}

type IndexMatch struct {
	index  int
	sample string
}
