package main

import (
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"log"
)

const PlainTest = "PlainTextValueTest"

// base64 valueS: Base64TestValue
const base64Test = "QmFzZTY0VGVzdFZhbHVlCg=="

const HexTest = "4865785465737456616c7565"

func main() {
	fmt.Println(PlainTest)
	b, err := base64.StdEncoding.DecodeString(base64Test)
	if err != nil {
		log.Fatalln(err)
	}
	fmt.Println(string(b))
	h, err := hex.DecodeString(HexTest)
	if err != nil {
		log.Fatalln(err)
	}
	fmt.Println(string(h))
}
